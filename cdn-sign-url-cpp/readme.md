Refer to URL:  https://cloud.google.com/cdn/docs/using-signed-urls. This is the sample code of Google Cloud CDN sign URL for C++ (CPP) for there is no this sample code in Google Cloud's official website. There are 2 kinds of Sign URL in this sample code: Full URL Sign URL and Prefix Sign URL.


Test OS: Debian


1. The signature of hmac-sha1 in the code depends on the lib implementation of openssl, so you need to download and compile the library of openssl 1.1.1

```
sudo apt-get install wget git make gcc g++ -y
wget https://www.openssl.org/source/openssl-1.1.1i.tar.gz
tar -zxvf openssl-1.1.1i.tar.gz
cd openssl-1.1.1i
./config   
sudo make install 
```

2. In Google Cloud CDN management cosole, add a sign key:

![image](./image/1.png){:height="50%" width="50%"}


3. git clone the source code from the repository:

```
git clone https://gitlab.com/zhmichael/google-cloud-demo.git
cd google-cloud-demo/cdn-sign-url-cpp/
```

4. Modify the url, key name, key value in sign_url.cpp

![image](./image/2.png)


5. Make and execute:

```
make
./signurl
```

![image](./image/3.png)  


